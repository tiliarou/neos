#include "stdafx.h"
#include "NEOS.hpp"

// wish I knew an easier way to make this file... there probably is one but I'm too dumb to know about it
// ah well it's here now, just have to make sure to keep it up to date
// last update: 1.2.0

std::map<int32_t, const char*> EResult_s = {
#undef EOS_RESULT_VALUE
#undef EOS_RESULT_VALUE_LAST
#define EOS_RESULT_VALUE(Name, Value) {Value, "Name"},
#define EOS_RESULT_VALUE_LAST(Name, Value) {Value, "Name"}
#include "eos_result.h"
#undef EOS_RESULT_VALUE
#undef EOS_RESULT_VALUE_LAST
};

// eos_auth_types.h
std::map<int32_t, const char*> ELoginCredentialType_s = {
	{0, "EOS_LCT_Password"},
	{1, "EOS_LCT_ExchangeCode"},
	{2, "EOS_LCT_DeviceAuth"},
	{3, "EOS_LCT_DeviceCode"},
	{4, "EOS_LCT_Developer"},
	{5, "EOS_LCT_RefreshToken"}, // 1.3.0
};

// eos_auth_types.h
std::map<int32_t, const char*> EAuthTokenType_s = {
	{0, "EOS_ATT_Client"},
	{1, "EOS_ATT_User"},
};

// eos_common.h
std::map<int32_t, const char*> ELoginStatus_s = {
	{0, "EOS_LS_NotLoggedIn"},
	{1, "EOS_LS_UsingLocalProfile"},
	{2, "EOS_LS_LoggedIn"},
};

// eos_connect_types.h
std::map<int32_t, const char*> EExternalCredentialType_s = {
	{0, "EOS_ECT_EPIC"},
	{1, "EOS_ECT_STEAM_APP_TICKET"},
};

// eos_connect_types.h
std::map<int32_t, const char*> EExternalAccountType_s = {
	{0, "EOS_EAT_EPIC"},
	{1, "EOS_EAT_STEAM"},
};

// eos_ecom_types.h
std::map<int32_t, const char*> EOwnershipStatus_s = {
	{0, "EOS_OS_NotOwned"},
	{1, "EOS_OS_Owned"},
};

// eos_ecom_types.h
std::map<int32_t, const char*> EEcomItemType_s = {
	{0, "EOS_EIT_Durable"},
	{1, "EOS_EIT_Consumable"},
	{2, "EOS_EIT_Other"},
};

// eos_friends_types.h
std::map<int32_t, const char*> EFriendsStatus_s = {
	{0, "EOS_FS_NotFriends"},
	{1, "EOS_FS_InviteSent"},
	{2, "EOS_FS_InviteReceived"},
	{3, "EOS_FS_Friends"},
};

// eos_logging.h
std::map<int32_t, const char*> ELogLevel_s = {
	{0, "EOS_LOG_Off"},
	{100, "EOS_LOG_Fatal"},
	{200, "EOS_LOG_Error"},
	{300, "EOS_LOG_Warning"},
	{400, "EOS_LOG_Info"},
	{500, "EOS_LOG_Verbose"},
	{600, "EOS_LOG_VeryVerbose"},
};

// eos_logging.h
std::map<int32_t, const char*> ELogCategory_s = {
	{0, "EOS_LC_Core"},
	{1, "EOS_LC_Auth"},
	{2, "EOS_LC_Friends"},
	{3, "EOS_LC_Presence"},
	{4, "EOS_LC_UserInfo"},
	{5, "EOS_LC_HttpSerialization"},
	{6, "EOS_LC_Ecom"},
	{7, "EOS_LC_P2P"},
	{8, "EOS_LC_Sessions"},
	{9, "EOS_LC_RateLimiter"},
	{10, "EOS_LC_PlayerDataStorage"}, // 1.3.0
	{11, "EOS_LC_Analytics"},
	{12, "EOS_LC_Messaging"},
	{13, "EOS_LC_Connect"},
	{14, "EOS_LC_Overlay"},
	{15, "EOS_LC_Achievements"}, // 1.3.0
	{16, "EOS_LC_Stats"}, // 1.3.0
	{17, "EOS_LC_UI"}, // 1.3.0
	{0x7fffffff, "EOS_LC_ALL_CATEGORIES"},
};

// eos_metrics_types.h
std::map<int32_t, const char*> EUserControllerType_s = {
	{0, "EOS_UCT_Unknown"},
	{1, "EOS_UCT_MouseKeyboard"},
	{2, "EOS_UCT_GamepadControl"},
	{3, "EOS_UCT_TouchControl"},
};

// eos_metrics_types.h
std::map<int32_t, const char*> EMetricsAccountIdType_s = {
	{0, "EOS_MAIT_Epic"},
	{1, "EOS_MAIT_External"},
};

// eos_p2p_types.h
std::map<int32_t, const char*> ENATType_s = {
	{0, "EOS_NAT_Unknown"},
	{1, "EOS_NAT_Open"},
	{2, "EOS_NAT_Moderate"},
	{3, "EOS_NAT_Strict"},
};

// eos_p2p_types.h
std::map<int32_t, const char*> EConnectionClosedReason_s = {
	{0, "EOS_CCR_Unknown"},
	{1, "EOS_CCR_ClosedByLocalUser"},
	{2, "EOS_CCR_ClosedByPeer"},
	{3, "EOS_CCR_TimedOut"},
	{4, "EOS_CCR_TooManyConnections"},
	{5, "EOS_CCR_InvalidMessage"},
	{6, "EOS_CCR_InvalidData"},
	{7, "EOS_CCR_ConnectionFailed"},
	{8, "EOS_CCR_ConnectionClosed"},
	{9, "EOS_CCR_NegotiationFailed"},
	{10, "EOS_CCR_UnexpectedError"},
};

// eos_presence_types.h
std::map<int32_t, const char*> Presence_EStatus_s = {
	{0, "EOS_PS_Offline"},
	{1, "EOS_PS_Online"},
	{2, "EOS_PS_Away"},
	{3, "EOS_PS_ExtendedAway"},
	{4, "EOS_PS_DoNotDisturb"},
};

// eos_sessions_types.h
std::map<int32_t, const char*> EOnlineSessionState_s = {
	{0, "EOS_OSS_NoSession"},
	{1, "EOS_OSS_Creating"},
	{2, "EOS_OSS_Pending"},
	{3, "EOS_OSS_Starting"},
	{4, "EOS_OSS_InProgress"},
	{5, "EOS_OSS_Ending"},
	{6, "EOS_OSS_Ended"},
	{7, "EOS_OSS_Destroying"},
};

// eos_sessions_types.h
std::map<int32_t, const char*> ESessionAttributeAdvertisementType_s = {
	{0, "EOS_SAAT_DontAdvertise"},
	{1, "EOS_SAAT_Advertise"},
};

// eos_sessions_types.h
std::map<int32_t, const char*> ESessionAttributeType_s = {
	{0, "EOS_SAT_Boolean"},
	{1, "EOS_SAT_Int64"},
	{2, "EOS_SAT_Double"},
	{3, "EOS_SAT_String"},
};

// eos_sessions_types.h
std::map<int32_t, const char*> EOnlineComparisonOp_s = {
	{0, "EOS_OCO_EQUAL"},
	{1, "EOS_OCO_NOTEQUAL"},
	{2, "EOS_OCO_GREATERTHAN"},
	{3, "EOS_OCO_GREATERTHANOREQUAL"},
	{4, "EOS_OCO_LESSTHAN"},
	{5, "EOS_OCO_LESSTHANOREQUAL"},
	{6, "EOS_OCO_DISTANCE"},
	{7, "EOS_OCO_ANYOF"},
	{8, "EOS_OCO_NOTANYOF"},
};

// eos_sessions_types.h
std::map<int32_t, const char*> EOnlineSessionPermissionLevel_s = {
	{0, "EOS_OSPF_PublicAdvertised"},
	{1, "EOS_OSPF_JoinViaPresence"},
	{2, "EOS_OSPF_InviteOnly"},
};

// eos_playerdatastorage_types.h (1.3.0)
std::map<int32_t, const char*> PlayerDataStorage_EReadResult_s = {
	{1, "EOS_RR_ContinueReading"},
	{2, "EOS_RR_FailRequest"},
	{3, "EOS_RR_CancelRequest"},
};

// eos_playerdatastorage_types.h (1.3.0)
std::map<int32_t, const char*> PlayerDataStorage_EWriteResult_s = {
	{1, "EOS_WR_ContinueWriting"},
	{2, "EOS_WR_CompleteRequest"},
	{3, "EOS_WR_FailRequest"},
	{4, "EOS_WR_CancelRequest"},
};

// map of EOSEnum -> enums string map + largest value
std::map<EOSEnum, std::pair<std::map<int32_t, const char*>*, int32_t>> EOSEnumProperties = {
	{ EOSEnum::EResult,								{ &EResult_s,								(int32_t)EOS_EResult::EOS_UnexpectedError } },
	{ EOSEnum::ELoginCredentialType,				{ &ELoginCredentialType_s,					(int32_t)EOS_ELoginCredentialType::EOS_LCT_Developer } },
	{ EOSEnum::EAuthTokenType,						{ &EAuthTokenType_s,						(int32_t)EOS_EAuthTokenType::EOS_ATT_User } },
	{ EOSEnum::ELoginStatus,						{ &ELoginStatus_s,							(int32_t)EOS_ELoginStatus::EOS_LS_LoggedIn } },
	{ EOSEnum::EExternalCredentialType,				{ &EExternalCredentialType_s,				(int32_t)EOS_EExternalCredentialType::EOS_ECT_STEAM_APP_TICKET } },
	{ EOSEnum::EExternalAccountType,				{ &EExternalAccountType_s,					(int32_t)EOS_EExternalAccountType::EOS_EAT_STEAM } },
	{ EOSEnum::EOwnershipStatus,					{ &EOwnershipStatus_s,						(int32_t)EOS_EOwnershipStatus::EOS_OS_Owned } },
	{ EOSEnum::EEcomItemType,						{ &EEcomItemType_s,							(int32_t)EOS_EEcomItemType::EOS_EIT_Other } },
	{ EOSEnum::EFriendsStatus,						{ &EFriendsStatus_s,						(int32_t)EOS_EFriendsStatus::EOS_FS_Friends } },
	{ EOSEnum::ELogLevel,							{ &ELogLevel_s,								(int32_t)EOS_ELogLevel::EOS_LOG_VeryVerbose } },
	{ EOSEnum::ELogCategory,						{ &ELogCategory_s,							(int32_t)EOS_ELogCategory::EOS_LC_ALL_CATEGORIES } },
	{ EOSEnum::EUserControllerType,					{ &EUserControllerType_s,					(int32_t)EOS_EUserControllerType::EOS_UCT_TouchControl } },
	{ EOSEnum::EMetricsAccountIdType,				{ &EMetricsAccountIdType_s,					(int32_t)EOS_EMetricsAccountIdType::EOS_MAIT_External } },
	{ EOSEnum::ENATType,							{ &ENATType_s,								(int32_t)EOS_ENATType::EOS_NAT_Strict } },
	{ EOSEnum::EConnectionClosedReason,				{ &EConnectionClosedReason_s,				(int32_t)EOS_EConnectionClosedReason::EOS_CCR_UnexpectedError } },
	{ EOSEnum::Presence_EStatus,					{ &Presence_EStatus_s,						(int32_t)EOS_Presence_EStatus::EOS_PS_DoNotDisturb } },
	{ EOSEnum::EOnlineSessionState,					{ &EOnlineSessionState_s,					(int32_t)EOS_EOnlineSessionState::EOS_OSS_Destroying } },
	{ EOSEnum::ESessionAttributeAdvertisementType,	{ &ESessionAttributeAdvertisementType_s,	(int32_t)EOS_ESessionAttributeAdvertisementType::EOS_SAAT_Advertise } },
	{ EOSEnum::ESessionAttributeType,				{ &ESessionAttributeType_s,					(int32_t)EOS_ESessionAttributeType::EOS_SAT_String } },
	{ EOSEnum::EOnlineComparisonOp,					{ &EOnlineComparisonOp_s,					(int32_t)EOS_EOnlineComparisonOp::EOS_OCO_NOTANYOF } },
	{ EOSEnum::EOnlineSessionPermissionLevel,		{ &EOnlineSessionPermissionLevel_s,			(int32_t)EOS_EOnlineSessionPermissionLevel::EOS_OSPF_InviteOnly } },

	{ EOSEnum::PlayerDataStorage_EReadResult, { &PlayerDataStorage_EReadResult_s, (int32_t)EOS_PlayerDataStorage_EReadResult::EOS_RR_CancelRequest } }, // 1.3.0
	{ EOSEnum::PlayerDataStorage_EWriteResult, { &PlayerDataStorage_EWriteResult_s, (int32_t)EOS_PlayerDataStorage_EWriteResult::EOS_WR_CancelRequest } }, // 1.3.0
};

const char* EnumToString(EOSEnum EnumType, int32_t Value)
{
	if (!EOSEnumProperties.count(EnumType))
		return "InvalidEnumType!";

	auto& properties = EOSEnumProperties[EnumType];
	auto* strings = properties.first;
	if (!strings)
		return "StringMapIsNull!";

	int32_t largest = properties.second;
	if (Value > largest)
		return "EnumValueOutOfBounds!";

	if(!strings->count(Value))
		return "EnumValueNameUnknown!";

	return (*strings)[Value];
}

const char* EResultToString(EOS_EResult Result)
{
	return EnumToString(EOSEnum::EResult, (int32_t)Result);
}
