#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * The Friends Interface is used to manage a user's friends list, by interacting with the backend services, and to retrieve the cached list of friends and pending invitations.
 * All Friends Interface calls take a handle of type EOS_HFriends as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetFriendsInterface function.
 *
 * @see EOS_Platform_GetFriendsInterface
 */

 /**
  * Starts an asynchronous task that reads the user's friends list from the backend service, caching it for future use.
  *
  * @param Options structure containing the account for which to retrieve the friends list
  * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
  * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
  */
EOS_DECLARE_FUNC(void) EOS_Friends_QueryFriends(EOS_HFriends Handle, const EOS_Friends_QueryFriendsOptions* Options, void* ClientData, const EOS_Friends_OnQueryFriendsCallback CompletionDelegate)
{
	if(Options)
		log(LL::Debug, "EOS_Friends_QueryFriends (%p(%p), %p, %p)", Options, Options->LocalUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Friends_QueryFriends (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_FRIENDS_QUERYFRIENDS_API_LATEST);

	PROXY_FUNC(EOS_Friends_QueryFriends);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Friends_QueryFriendsOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Friends_QueryFriendsCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;

			CompletionDelegate(&cbi);
		});
}

/**
 * Starts an asynchronous task that sends a friend invitation to another user. The completion delegate is executed after the backend response has been received.
 * It does not indicate that the target user has responded to the friend invitation.
 *
 * @param Options structure containing the account to send the invite from and the account to send the invite to
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Friends_SendInvite(EOS_HFriends Handle, const EOS_Friends_SendInviteOptions* Options, void* ClientData, const EOS_Friends_OnSendInviteCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_Friends_SendInvite (%p(%p, %p), %p, %p)", Options, Options->LocalUserId, Options->TargetUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Friends_SendInvite (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_FRIENDS_SENDINVITE_API_LATEST);

	PROXY_FUNC(EOS_Friends_SendInvite);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Friends_SendInviteOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Friends_SendInviteCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.TargetUserId = Options ? options.TargetUserId : nullptr;

			CompletionDelegate(&cbi);
		});
}

/**
 * Starts an asynchronous task that accepts a friend invitation from another user. The completion delegate is executed after the backend response has been received.
 *
 * @param Options structure containing the logged in account and the inviting account
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Friends_AcceptInvite(EOS_HFriends Handle, const EOS_Friends_AcceptInviteOptions* Options, void* ClientData, const EOS_Friends_OnAcceptInviteCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_Friends_AcceptInvite (%p(%p, %p), %p, %p)", Options, Options->LocalUserId, Options->TargetUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Friends_AcceptInvite (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_FRIENDS_ACCEPTINVITE_API_LATEST);

	PROXY_FUNC(EOS_Friends_AcceptInvite);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Friends_AcceptInviteOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Friends_AcceptInviteCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.TargetUserId = Options ? options.TargetUserId : nullptr;

			CompletionDelegate(&cbi);
		});
}

/**
 * Starts an asynchronous task that rejects a friend invitation from another user. The completion delegate is executed after the backend response has been received.
 *
 * @param Options structure containing the logged in account and the inviting account
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Friends_RejectInvite(EOS_HFriends Handle, const EOS_Friends_RejectInviteOptions* Options, void* ClientData, const EOS_Friends_OnRejectInviteCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_Friends_RejectInvite (%p(%p, %p), %p, %p)", Options, Options->LocalUserId, Options->TargetUserId, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Friends_RejectInvite (%p, %p, %p)", Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_FRIENDS_REJECTINVITE_API_LATEST);

	PROXY_FUNC(EOS_Friends_RejectInvite);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_Friends_RejectInviteOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_Friends_RejectInviteCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.TargetUserId = Options ? options.TargetUserId : nullptr;

			CompletionDelegate(&cbi);
		});
}

/**
 * Retrieves the number of friends on the friends list that has already been retrieved by the EOS_Friends_QueryFriends API.
 *
 * @param Options structure containing the account id of the friends list
 * @return the number of friends on the list
 *
 * @see EOS_Friends_GetFriendAtIndex
 */
EOS_DECLARE_FUNC(int32_t) EOS_Friends_GetFriendsCount(EOS_HFriends Handle, const EOS_Friends_GetFriendsCountOptions* Options)
{
	if(Options)
		log(LL::Debug, "EOS_Friends_GetFriendsCount (%p(%p))", Options, Options->LocalUserId);
	else
		log(LL::Debug, "EOS_Friends_GetFriendsCount (%p)", Options);

	EOS_CHECK_VERSION(EOS_FRIENDS_GETFRIENDSCOUNT_API_LATEST);

	PROXY_FUNC(EOS_Friends_GetFriendsCount);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	return 0;
}

/**
 * Retrieves the account id of an entry from the friends list that has already been retrieved by the EOS_Friends_QueryFriends API.
 * The account id returned by this function may belong to an account that has been invited to be a friend or that has invited the local user to be a friend.
 * To determine if the account id returned by this function is a friend or a pending friend invitation, use the EOS_Friends_GetStatus function.
 *
 * @param Options structure containing the account id of the friends list and the index into the list
 * @return the account id of the friend. Note that if the index provided is out of bounds, the returned account id will be a "null" account id.
 *
 * @see EOS_Friends_GetFriendsCount
 * @see EOS_Friends_GetStatus
 */
EOS_DECLARE_FUNC(EOS_EpicAccountId) EOS_Friends_GetFriendAtIndex(EOS_HFriends Handle, const EOS_Friends_GetFriendAtIndexOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_Friends_GetFriendAtIndex (%p(%p, %d))", Options, Options->LocalUserId, Options->Index);
	else
		log(LL::Debug, "EOS_Friends_GetFriendAtIndex (%p)", Options);

	EOS_CHECK_VERSION(EOS_FRIENDS_GETFRIENDATINDEX_API_LATEST);

	PROXY_FUNC(EOS_Friends_GetFriendAtIndex);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return nullptr;
}

/**
 * Retrieve the friendship status between the local user and another user.
 *
 * @param Options structure containing the account id of the friend list to check and the account of the user to test friendship status
 * @return A value indicating whether the two accounts have a friendship, pending invites in either direction, or no relationship
 *         EOS_FS_Friends is returned for two users that have confirmed friendship
 *         EOS_FS_InviteSent is returned when the local user has sent a friend invitation but the other user has not accepted or rejected it
 *         EOS_FS_InviteReceived is returned when the other user has sent a friend invitation to the local user
 *         EOS_FS_NotFriends is returned when there is no known relationship
 *
 * @see EOS_EFriendsStatus
 */
EOS_DECLARE_FUNC(EOS_EFriendsStatus) EOS_Friends_GetStatus(EOS_HFriends Handle, const EOS_Friends_GetStatusOptions* Options)
{
	if (Options)
		log(LL::Debug, "EOS_Friends_GetStatus (%p(%p, %p))", Options, Options->LocalUserId, Options->TargetUserId);
	else
		log(LL::Debug, "EOS_Friends_GetStatus (%p)", Options);

	EOS_CHECK_VERSION(EOS_FRIENDS_GETSTATUS_API_LATEST);

	PROXY_FUNC(EOS_Friends_GetStatus);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %s (%d)", EnumToString(EOSEnum::EFriendsStatus, (int32_t)res), res);
		return res;
	}

	return EOS_EFriendsStatus::EOS_FS_NotFriends;
}
