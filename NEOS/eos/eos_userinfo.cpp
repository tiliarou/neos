#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * Release the memory associated with an EOS_UserInfo structure. This must be called on data retrieved from EOS_UserInfo_CopyUserInfo.
 *
 * @param UserInfo - The user info structure to release
 *
 * @see EOS_UserInfo
 * @see EOS_UserInfo_CopyUserInfo
 */
EOS_DECLARE_FUNC(void) EOS_UserInfo_Release(EOS_UserInfo* UserInfo)
{
	log(LL::Debug, "EOS_UserInfo_Release (%p)", UserInfo);

	PROXY_FUNC(EOS_UserInfo_Release);
	if (proxied)
	{
		proxied(UserInfo);
		return;
	}

	if (!UserInfo)
		return;

	delete UserInfo;
}

/**
 * The UserInfo Interface is used to receive user information for account IDs from the backend services and to retrieve that information once it is cached.
 * All UserInfo Interface calls take a handle of type EOS_HUserInfo as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetUserInfoInterface function.
 *
 * @see EOS_Platform_GetUserInfoInterface
 */

 /**
  * EOS_UserInfo_QueryUserInfo is used to start an asynchronous query to retrieve information, such as display name, about another account.
  * Once the callback has been fired with a successful ResultCode, it is possible to call EOS_UserInfo_CopyUserInfo to receive an EOS_UserInfo containing the available information.
  *
  * @param Options structure containing the input parameters
  * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
  * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
  *
  * @see EOS_UserInfo
  * @see EOS_UserInfo_CopyUserInfo
  * @see EOS_UserInfo_QueryUserInfoOptions
  * @see EOS_UserInfo_OnQueryUserInfoCallback
  */
EOS_DECLARE_FUNC(void) EOS_UserInfo_QueryUserInfo(EOS_HUserInfo Handle, const EOS_UserInfo_QueryUserInfoOptions* Options, void* ClientData, const EOS_UserInfo_OnQueryUserInfoCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_UserInfo_QueryUserInfo (%p(%p, %p))", Options, Options->LocalUserId, Options->TargetUserId);
	else
		log(LL::Debug, "EOS_UserInfo_QueryUserInfo (%p)", Options);

	EOS_CHECK_VERSION(EOS_USERINFO_QUERYUSERINFO_API_LATEST);

	PROXY_FUNC(EOS_UserInfo_QueryUserInfo);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_UserInfo_QueryUserInfoOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_UserInfo_QueryUserInfoCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.TargetUserId = Options ? options.TargetUserId : nullptr;
			if (Options && options.TargetUserId != UserAccountId)
				cbi.ResultCode = EOS_EResult::EOS_NotFound;

			CompletionDelegate(&cbi);
		});
}

/**
 * EOS_UserInfo_QueryUserInfoByDisplayName is used to start an asynchronous query to retrieve user information by display name. This can be useful for getting the EOS_AccountId for a display name.
 * Once the callback has been fired with a successful ResultCode, it is possible to call EOS_UserInfo_CopyUserInfo to receive an EOS_UserInfo containing the available information.
 *
 * @param Options structure containing the input parameters
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the async operation completes, either successfully or in error
 *
 * @see EOS_UserInfo
 * @see EOS_UserInfo_CopyUserInfo
 * @see EOS_UserInfo_QueryUserInfoByDisplayNameOptions
 * @see EOS_UserInfo_OnQueryUserInfoByDisplayNameCallback
 */
EOS_DECLARE_FUNC(void) EOS_UserInfo_QueryUserInfoByDisplayName(EOS_HUserInfo Handle, const EOS_UserInfo_QueryUserInfoByDisplayNameOptions* Options, void* ClientData, const EOS_UserInfo_OnQueryUserInfoByDisplayNameCallback CompletionDelegate)
{
	if (Options)
		log(LL::Debug, "EOS_UserInfo_QueryUserInfoByDisplayName (%p(%s))", Options, Options->DisplayName);
	else
		log(LL::Debug, "EOS_UserInfo_QueryUserInfoByDisplayName (%p)", Options);

	EOS_CHECK_VERSION(EOS_USERINFO_QUERYUSERINFOBYDISPLAYNAME_API_LATEST);

	PROXY_FUNC(EOS_UserInfo_QueryUserInfoByDisplayName);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	// Make a copy of Options as some games will free it straight after this function
	EOS_UserInfo_QueryUserInfoByDisplayNameOptions options;
	if (Options)
		options = *Options;

	NEOS_AddCallback([options, Options, ClientData, CompletionDelegate]()
		{
			EOS_UserInfo_QueryUserInfoByDisplayNameCallbackInfo cbi;
			cbi.ResultCode = EOS_EResult::EOS_NotFound; //Options != nullptr ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = Options ? options.LocalUserId : nullptr;
			cbi.DisplayName = Options ? options.DisplayName : nullptr;
			cbi.TargetUserId = nullptr;

			CompletionDelegate(&cbi);
		});
}

/**
 * EOS_UserInfo_CopyUserInfo is used to immediately retrieve a copy of user information for an account ID, cached by a previous call to EOS_UserInfo_QueryUserInfo.
 * If the call returns an EOS_Success result, the out parameter, OutUserInfo, must be passed to EOS_UserInfo_Release to release the memory associated with it.
 *
 * @param Options structure containing the input parameters
 * @param OutUserInfo out parameter used to receive the EOS_UserInfo structure.
 *
 * @return EOS_Success if the information is available and passed out in OutUserInfo
 *         EOS_InvalidParameters if you pass a null pointer for the out parameter
 *         EOS_IncompatibleVersion if the API version passed in is incorrect
 *         EOS_NotFound if the user info is not locally cached. The information must have been previously cached by a call to EOS_UserInfo_QueryUserInfo
 *
 * @see EOS_UserInfo
 * @see EOS_UserInfo_CopyUserInfoOptions
 * @see EOS_UserInfo_Release
 */
EOS_DECLARE_FUNC(EOS_EResult) EOS_UserInfo_CopyUserInfo(EOS_HUserInfo Handle, const EOS_UserInfo_CopyUserInfoOptions* Options, EOS_UserInfo** OutUserInfo)
{
	if (Options)
		log(LL::Debug, "EOS_UserInfo_CopyUserInfo (%p(%p))", Options, Options->TargetUserId);
	else
		log(LL::Debug, "EOS_UserInfo_CopyUserInfo (%p)", Options);

	EOS_CHECK_VERSION(EOS_USERINFO_COPYUSERINFO_API_LATEST);

	PROXY_FUNC(EOS_UserInfo_CopyUserInfo);
	if (proxied)
	{
		auto res = proxied(Handle, Options, OutUserInfo);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if(!Options)
		return EOS_EResult::EOS_InvalidParameters;

	if (Options->TargetUserId != UserAccountId)
		return EOS_EResult::EOS_NotFound;

	*OutUserInfo = new EOS_UserInfo;
	(*OutUserInfo)->ApiVersion = Options->ApiVersion;
	(*OutUserInfo)->UserId = Options->TargetUserId;
	(*OutUserInfo)->Country = UserCountry.c_str();
	(*OutUserInfo)->DisplayName = UserName.c_str();
	(*OutUserInfo)->PreferredLanguage = UserLanguage.c_str();

	return EOS_EResult::EOS_Success;
}
