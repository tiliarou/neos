#pragma once
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <cstdint>
#include <string>
#include <vector>
#include <deque>
#include <algorithm>
#include <functional>

#include <fstream>
#include <sstream>
#include <mutex>
#include <cwctype>
#include <filesystem>

#include "3rdparty/fifo_map.hpp"
using nlohmann::fifo_map;

#include "eos_sdk.h"
#if !defined(EOS_BUILD_120) && !defined(EOS_BUILD_130)
#include "eos_auth_types.h" // don't include function defs in 1.1.0, since some were changed in 1.2.0
#else
#include "eos_auth.h"
#endif
#ifdef EOS_BUILD_120
#include "eos_connect.h"
#endif
#include "eos_common.h"
#include "eos_ecom.h"
#include "eos_friends.h"
#include "eos_init.h"
#include "eos_types.h"
#include "eos_logging.h"
#include "eos_metrics.h"
#include "eos_presence.h"
#ifdef EOS_BUILD_120
#include "eos_sessions.h"
#endif
#include "eos_userinfo.h"
#ifdef EOS_BUILD_120
#include "eos_p2p.h"
#endif
#ifdef EOS_BUILD_130
#include "eos_achievements.h"
#include "eos_playerdatastorage.h"
#include "eos_stats.h"
#endif

// Sets "proxied" variable to non-null function ptr if ProxyLibrary is loaded and we found the functions address
#define PROXY_FUNC(func) typedef decltype(&func) func##_f; \
    static func##_f proxied = nullptr; \
	if (ProxyLibrary && !proxied) { \
		proxied = (func##_f)GetProcAddress(ProxyLibrary, #func); \
		if(!proxied) \
			log(LL::Error, "Failed to proxy function " #func); \
	}

// helper define for use in functions that return EOS_EResult
#define EOS_CHECK_CONFIGURED() do { \
if (!EOS_IsConfigured()) \
	return EOS_EResult::EOS_NotConfigured; \
} while(0);

// Checks against Options->ApiVersion and warns user if version is newer
#define EOS_CHECK_VERSION(max_version) do { \
	if (Options && Options->ApiVersion > max_version) \
	{ \
		static bool warned = false; \
		if (!warned) \
		{ \
			log(LL::Warning, __FUNCTION__ ": game uses newer API %d than what nEOS is built for (%d)!", Options->ApiVersion, max_version); \
			warned = true; \
		} \
	} \
} while(0);