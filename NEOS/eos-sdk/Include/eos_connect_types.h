// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include "eos_common.h"

#pragma pack(push, 8)

EXTERN_C typedef struct EOS_ConnectHandle* EOS_HConnect;

/** Max length of an external account id in string form */
#define EOS_CONNECT_EXTERNAL_ACCOUNT_ID_MAX_LENGTH 256

/**
 * List of the supported identity providers to authenticate a user.
 *
 * The type of authentication token is specific to each provider.
 * Tokens in string format should be passed as-is to the function,
 * and tokens retrieved as raw byte arrays should be converted to string using
 * the EOS_ByteArray_ToString API before passing them to the EOS_Connect API.
 *
 * @see EOS_Connect_Login
 * @see EOS_Connect_Credentials
 */
EOS_ENUM(EOS_EExternalCredentialType,
	/** External token is from Epic Games Account Service */
	EOS_ECT_EPIC = 0,
	/**
	 * Steam Encrypted App Ticket.
	 *
	 * Generated using the Steamworks SDK's ISteamUser::GetEncryptedAppTicket API.
	 *
	 * Use the EOS_ByteArray_ToString API to pass the token as a hex-encoded string.
	 */
	EOS_ECT_STEAM_APP_TICKET = 1
);

/**
 * All supported external account providers
 *
 * @see EOS_Connect_QueryAccountMappings
 */
EOS_ENUM(EOS_EExternalAccountType,
	/** External account is associated with the Epic Games Account Service */
	EOS_EAT_EPIC = 0,
	/** External account is associated with Steam */
	EOS_EAT_STEAM = 1
);

/** The most recent version of the EOS_Connect_Credentials struct. */
#define EOS_CONNECT_CREDENTIALS_API_LATEST 1

/**
 * A structure that contains external login credentials.
 * 
 * This is part of the input structure EOS_Connect_LoginOptions
 *
 * @see EOS_EExternalCredentialType
 * @see EOS_Connect_Login
 */ 
EOS_STRUCT(EOS_Connect_Credentials, (
	/** Version of the API */
	int32_t ApiVersion;
	/** External token associated with the user logging in */
	const char* Token;
	/** Type of external login. Needed to identify the auth method to use */
	EOS_EExternalCredentialType Type;
));

/** The most recent version of the EOS_Connect_Login API. */
#define EOS_CONNECT_LOGIN_API_LATEST 1

/**
 * Input parameters for the EOS_Connect_Login Function.
 */
EOS_STRUCT(EOS_Connect_LoginOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Credentials specified for a given login method */
	const EOS_Connect_Credentials* Credentials;
));

/**
 * Output parameters for the EOS_Connect_Login Function.
 */
EOS_STRUCT(EOS_Connect_LoginCallbackInfo, (
	/** Result code for the operation. EOS_Success is returned for a successful query, otherwise one of the error codes is returned. See eos_common.h */
	EOS_EResult ResultCode;
	/** Context that was passed into EOS_Connect_Login */
	void* ClientData;
	/** If login was succesful, this is the account ID of the local player that logged in */
	EOS_ProductUserId LocalUserId;
	/** 
	 * If the user was not found with credentials passed into EOS_Connect_Login, 
	 * this continuance token can be passed to either EOS_Connect_CreateUser 
	 * or EOS_Connect_LinkAccount to continue the flow
	 */
	EOS_ContinuanceToken ContinuanceToken;
));

/**
 * Function prototype definition for callbacks passed to EOS_Connect_Login
 * @param Data A EOS_Connect_LoginCallbackInfo containing the output information and result
 */
EOS_DECLARE_CALLBACK(EOS_Connect_OnLoginCallback, const EOS_Connect_LoginCallbackInfo* Data);

/** The most recent version of the EOS_Connect_CreateUser API. */
#define EOS_CONNECT_CREATEUSER_API_LATEST 1

/**
 * Input parameters for the EOS_Connect_CreateUser Function.
 */
EOS_STRUCT(EOS_Connect_CreateUserOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Continuance token from previous call to EOS_Connect_Login */
	EOS_ContinuanceToken ContinuanceToken;
));

/**
 * Output parameters for the EOS_Connect_CreateUser Function.
 */
EOS_STRUCT(EOS_Connect_CreateUserCallbackInfo, (
	/** Result code for the operation. EOS_Success is returned for a successful query, otherwise one of the error codes is returned. See eos_common.h */
	EOS_EResult ResultCode;
	/** Context that was passed into EOS_Connect_CreateUser */
	void* ClientData;
	/** Account ID of the local player created by this operation */
	EOS_ProductUserId LocalUserId;
));

EOS_DECLARE_CALLBACK(EOS_Connect_OnCreateUserCallback, const EOS_Connect_CreateUserCallbackInfo* Data);

/** The most recent version of the EOS_Connect_LinkAccount API. */
#define EOS_CONNECT_LINKACCOUNT_API_LATEST 1

/**
 * Input parameters for the EOS_Connect_LinkAccount Function.
 */
EOS_STRUCT(EOS_Connect_LinkAccountOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Existing logged in user that will link to the external account referenced by the continuance token */
	EOS_ProductUserId LocalUserId;
	/** Continuance token from previous call to EOS_Connect_Login */
	EOS_ContinuanceToken ContinuanceToken;
));

/**
 * Output parameters for the EOS_Connect_LinkAccount Function.
 */
EOS_STRUCT(EOS_Connect_LinkAccountCallbackInfo, (
	/** Result code for the operation. EOS_Success is returned for a successful query, otherwise one of the error codes is returned. See eos_common.h */
	EOS_EResult ResultCode;
	/** Context that was passed into EOS_Connect_LinkAccount */
	void* ClientData;
	/** Existing logged in user that had external auth linked */
	EOS_ProductUserId LocalUserId;
));

/**
 * Function prototype definition for callbacks passed to EOS_Connect_LinkAccount
 * @param Data A EOS_Connect_LinkAccountCallbackInfo containing the output information and result
 */
EOS_DECLARE_CALLBACK(EOS_Connect_OnLinkAccountCallback, const EOS_Connect_LinkAccountCallbackInfo* Data);

/** The most recent version of the EOS_Connect_QueryExternalAccountMappings API. */
#define EOS_CONNECT_QUERYEXTERNALACCOUNTMAPPINGS_API_LATEST 1

/** Maximum number of account ids that can be queried at once */
#define EOS_CONNECT_QUERYEXTERNALACCOUNTMAPPINGS_MAX_ACCOUNT_IDS 128

/**
 * Input parameters for the EOS_Connect_QueryExternalAccountMappings Function.
 */
EOS_STRUCT(EOS_Connect_QueryExternalAccountMappingsOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Existing logged in user that is querying account mappings */
	EOS_ProductUserId LocalUserId;
	/** External auth service supplying the account ids in string form */
	EOS_EExternalAccountType AccountIdType;
	/** An array of account ids to map to product user id */
	const char** ExternalAccountIds;
	/** Number of account ids to query */
	uint32_t ExternalAccountIdCount;
));

/**
 * Output parameters for the EOS_Connect_QueryExternalAccountMappings Function.
 */
EOS_STRUCT(EOS_Connect_QueryExternalAccountMappingsCallbackInfo, (
	/** Result code for the operation. EOS_Success is returned for a successful query, otherwise one of the error codes is returned. See eos_common.h */
	EOS_EResult ResultCode;
	/** Context that was passed into EOS_Connect_QueryExternalAccountMappings */
	void* ClientData;
	/** Existing logged in user that made the request */
	EOS_ProductUserId LocalUserId;
));

/**
 * Function prototype definition for callbacks passed to EOS_Connect_QueryExternalAccountMappings
 * @param Data A EOS_Connect_QueryExternalAccountMappingsCallbackInfo containing the output information and result
 */
EOS_DECLARE_CALLBACK(EOS_Connect_OnQueryExternalAccountMappingsCallback, const EOS_Connect_QueryExternalAccountMappingsCallbackInfo* Data);

/** The most recent version of the EOS_Connect_GetExternalAccountMappings API. */
#define EOS_CONNECT_GETEXTERNALACCOUNTMAPPINGS_API_LATEST 1

/**
 * Input parameters for the EOS_Connect_GetExternalAccountMappings Function.
 */
EOS_STRUCT(EOS_Connect_GetExternalAccountMappingsOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Existing logged in user that is querying account mappings */
	EOS_ProductUserId LocalUserId;
	/** External auth service supplying the account ids in string form */
	EOS_EExternalAccountType AccountIdType;
	/** Target user to retrieve the mapping for, as an external account id */
	const char* TargetExternalUserId;
));

/** The most recent version of the EOS_Connect_QueryProductUserIdMappings API. */
#define EOS_CONNECT_QUERYPRODUCTUSERIDMAPPINGS_API_LATEST 1

/** Maximum number of account ids that can be queried at once */
#define EOS_CONNECT_QUERYPRODUCTUSERIDMAPPINGS_MAX_ACCOUNT_IDS 128

/**
 * Input parameters for the EOS_Connect_QueryProductUserIdMapping Function.
 */
EOS_STRUCT(EOS_Connect_QueryProductUserIdMappingsOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Existing logged in user that is querying account mappings */
	EOS_ProductUserId LocalUserId;
	/** External auth service mapping to retrieve */
	EOS_EExternalAccountType AccountIdType;
	/** An array of account ids to map to product user id */
	EOS_ProductUserId* ProductUserIds;
	/** Number of account ids to query */
	uint32_t ProductUserIdCount;
));

/**
 * Output parameters for the EOS_Connect_QueryProductUserIdMappings Function.
 */
EOS_STRUCT(EOS_Connect_QueryProductUserIdMappingsCallbackInfo, (
	/** Result code for the operation. EOS_Success is returned for a successful query, otherwise one of the error codes is returned. See eos_common.h */
	EOS_EResult ResultCode;
	/** Context that was passed into EOS_Connect_QueryProductUserIdMappings */
	void* ClientData;
	/** Existing logged in user that made the request */
	EOS_ProductUserId LocalUserId;
));

/**
 * Function prototype definition for callbacks passed to EOS_Connect_QueryProductUserIdMappings
 * @param Data A EOS_Connect_QueryProductUserIdMappingsCallbackInfo containing the output information and result
 */
EOS_DECLARE_CALLBACK(EOS_Connect_OnQueryProductUserIdMappingsCallback, const EOS_Connect_QueryProductUserIdMappingsCallbackInfo* Data);

/** The most recent version of the EOS_Connect_GetProductUserIdMapping API. */
#define EOS_CONNECT_GETPRODUCTUSERIDMAPPING_API_LATEST 1

/**
 * Input parameters for the EOS_Connect_GetProductUserIdMapping Function.
 */
EOS_STRUCT(EOS_Connect_GetProductUserIdMappingOptions, (
	/** Version of the API */
	int32_t ApiVersion;
	/** Existing logged in user that is querying account mappings */
	EOS_ProductUserId LocalUserId;
	/** External auth service mapping to retrieve */
	EOS_EExternalAccountType AccountIdType;
	/** Target product user id to retrieve */
	EOS_ProductUserId TargetProductUserId;
));

/** The most recent version of the EOS_Connect_AddNotifyAuthExpiration API. */
#define EOS_CONNECT_ADDNOTIFYAUTHEXPIRATION_API_LATEST 1
/**
 * Structure containing information for the auth expiration notification callback
 */
EOS_STRUCT(EOS_Connect_AddNotifyAuthExpirationOptions, (
	/** API Version of the EOS_Connect_AddNotifyAuthExpirationOptions structure */
	int32_t ApiVersion;
));

/** The most recent version of the EOS_Connect_OnAuthExpirationCallback API. */
#define EOS_CONNECT_ONAUTHEXPIRATIONCALLBACK_API_LATEST 1

/**
 * Output parameters for the EOS_Connect_OnAuthExpirationCallback Function.
 */
EOS_STRUCT(EOS_Connect_AuthExpirationCallbackInfo, (
	/** Context that was passed into EOS_Connect_AddNotifyAuthExpiration */
	void* ClientData;
	/** Account ID of the local player whose status has changed */
	EOS_ProductUserId LocalUserId;
));

/**
 * Function prototype definition for notifications that come from EOS_Connect_AddNotifyAuthExpiration
 *
 * @param Data A EOS_Connect_AuthExpirationCallbackInfo containing the output information and result
 */
EOS_DECLARE_CALLBACK(EOS_Connect_OnAuthExpirationCallback, const EOS_Connect_AuthExpirationCallbackInfo* Data);


/** The most recent version of the EOS_Connect_AddNotifyLoginStatusChangedOptions API. */
#define EOS_CONNECT_ADDNOTIFYLOGINSTATUSCHANGED_API_LATEST 1
/**
 * Structure containing information or the connect user login status change callback
 */
EOS_STRUCT(EOS_Connect_AddNotifyLoginStatusChangedOptions, (
	/** API Version of the EOS_Connect_AddNotifyLoginStatusChangedOptions structure */
	int32_t ApiVersion;
));

/**
 * Output parameters for the EOS_Connect_OnLoginStatusChangedCallback Function.
 */
EOS_STRUCT(EOS_Connect_LoginStatusChangedCallbackInfo, (
	/** Context that was passed into EOS_Connect_AddNotifyLoginStatusChanged */
	void* ClientData;
	/** Account ID of the local player whose status has changed */
	EOS_ProductUserId LocalUserId;
	/** The status prior to the change */
	EOS_ELoginStatus PreviousStatus;
	/** The status at the time of the notification */
	EOS_ELoginStatus CurrentStatus;
));

/**
 * Function prototype definition for notifications that come from EOS_Connect_AddNotifyLoginStatusChanged
 *
 * @param Data A EOS_Connect_LoginStatusChangedCallbackInfo containing the output information and result
 */
EOS_DECLARE_CALLBACK(EOS_Connect_OnLoginStatusChangedCallback, const EOS_Connect_LoginStatusChangedCallbackInfo* Data);

#pragma pack(pop)
