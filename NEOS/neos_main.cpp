#include "stdafx.h"
#include "NEOS.hpp"

// TODO:
// 0.9:
// - move code into classes? (HPlatform/HEcon etc?)

#include "3rdparty/ini.h"

#define NEOS_VERSION "1.3.1-alpha"

bool EOSIsInited = false;
bool EOSIsShutdown = false;

std::deque<std::function<void()>> EOSCallbacks;
std::mutex EOSCallbacksMutex;

// Runtime stuff
HMODULE NEOSLibrary = nullptr;
HMODULE ProxyLibrary = nullptr;

std::filesystem::path NEOSFolder;

// Gets set from calling EXE
std::string GameProductName;
std::string GameProductVersion;
std::string GameProductId;
std::string GameSandboxId;
std::string GameClientId;
std::string GameClientSecret;

// INI configurable settings

// some games have trouble with async callbacks, this can let us disable them and make callbacks synchronous (todo: remove this now async is fixed?)
bool UseAsyncCallbacks = true;
bool ProxyHookLogs = false;
std::string ProxyFilename = "";

std::string UserName = "nEOSUser";
EOS_EpicAccountId UserAccountId = reinterpret_cast<EOS_EpicAccountId>(0x1234);
EOS_ProductUserId UserProductId = reinterpret_cast<EOS_ProductUserId>(0x5678); // TODO120
std::string UserLanguage = "en";
std::string UserCountry = "US";

std::string IniGameProductId;

std::string OverrideProductName;
std::string OverrideProductVersion;
std::string OverrideProductId;
std::string OverrideSandboxId;
std::string OverrideClientId;
std::string OverrideClientSecret;
std::string OverrideEncryptionKey;
std::string OverrideDeploymentId;

bool DLCLogQueries = true;
bool DLCAllOwned = false;
fifo_map<std::string, std::string> DLCOwned;
fifo_map<std::string, std::string> DLCForced;

std::vector<std::pair<std::string, std::string>> Entitlements;

bool ForcedDLCUseMalloc = false;

// Builds a command-line string from the info we have (INI/game provided), as some games seem to require it
std::wstring NEOS_CommandLine(const std::wstring& ModulePath)
{
	std::wstringstream ws;
	// argv[0] has to be module path
	ws << L"\"";
	ws << ModulePath;
	ws << L"\" ";

	ws << L"-AUTH_LOGIN=unused -AUTH_PASSWORD=unused -AUTH_TYPE=exchangecode -epicapp=";
	ws << WidenString(!GameProductId.empty() ? GameProductId : IniGameProductId);
	ws << L" -epicenv=Prod -EpicPortal -epicusername=\"";
	ws << WidenString(UserName);
	ws << L"\" -epicuserid=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa -epiclocale=";

	if (!UserLanguage.empty())
		ws << WidenString(UserLanguage);
	if (!UserLanguage.empty() && !UserCountry.empty())
		ws << L"-";
	if (!UserCountry.empty())
		ws << WidenString(UserCountry);

	// Put old command-line at the end - this should let it override anything we added
	auto OldCommandLine = GetCommandLineNoExe();
	if(wcslen(OldCommandLine))
		ws << L" " << GetCommandLineNoExe();

	return ws.str();
}

bool NEOS_RestartAppIfNecessary()
{
	std::wstring CommandLine = std::wstring(GetCommandLineW());
	std::transform(CommandLine.begin(), CommandLine.end(), CommandLine.begin(), std::towlower);

	if (CommandLine.find(L"-epicapp=") != std::wstring::npos)
		return false;

	log(LL::Debug, "Setting up command-line & relaunching...");

	HMODULE hModule = GetModuleHandleW(NULL);
	WCHAR ModulePath[4096];
	GetModuleFileNameW(hModule, ModulePath, 4096);

	auto NewCommandLine = NEOS_CommandLine(ModulePath);

	log(LL::Debug, "ModulePath = %ws", ModulePath);
	log(LL::Debug, "CommandLine = %ws", NewCommandLine.c_str());

	PROCESS_INFORMATION ProcInfo = { 0 };
	STARTUPINFOW StartupInfo = { 0 };
	StartupInfo.cb = sizeof(STARTUPINFOW);

#pragma warning( push )
#pragma warning( disable : 6335 )
	BOOL res = CreateProcessW(ModulePath, (LPWSTR)NewCommandLine.c_str(), 0, 0, false, 0, 0, 0, &StartupInfo, &ProcInfo);
#pragma warning( pop )

	log(LL::Debug, "CreateProcessW result %d", res);

	exit(0);

	return true;
}

void NEOS_AddCallback(std::function<void()> Func)
{
	if (!UseAsyncCallbacks)
	{
		Func();
		return;
	}

	std::lock_guard<std::mutex> guard(EOSCallbacksMutex);
	EOSCallbacks.push_back(Func);
}

void NEOS_RunCallbacks()
{
	if (!UseAsyncCallbacks)
		return;

	std::unique_lock<std::mutex> lck(EOSCallbacksMutex);
	while (!EOSCallbacks.empty())
	{
		auto fn = EOSCallbacks.front();
		EOSCallbacks.pop_front();
		lck.unlock(); // unlock before running callback, so callback is able to modify callback vector
		fn();
		lck.lock(); // relock afterward
	}
}

int NEOS_IniHandler(void* user, const char* section_raw,
	const char* name_raw, const char* value)
{
	std::string section = section_raw;
	std::string name = name_raw;

	if (section == "nEOS" && name == "Logging")
		LogEnabled = StringToBool(value);
	else if (section == "nEOS" && name == "LogFile")
		LogFilename = value;
	else if (section == "nEOS" && name == "LogLevel")
		LogLevel = std::strtol(value, 0, 0);
	else if (section == "nEOS" && name == "ProxyFilename")
		ProxyFilename = value;
	else if (section == "nEOS" && name == "UseAsyncCallbacks")
		UseAsyncCallbacks = StringToBool(value);
	else if (section == "nEOS" && name == "ProxyHookLogs")
		ProxyHookLogs = StringToBool(value);
	else if (section == "User" && name == "UserName")
		UserName = value;
	else if (section == "User" && name == "AccountId")
		UserAccountId = reinterpret_cast<EOS_EpicAccountId>(std::strtoull(value, 0, 0));
	else if (section == "User" && name == "ProductId")
		UserProductId = reinterpret_cast<EOS_ProductUserId>(std::strtoull(value, 0, 0));
	else if (section == "User" && name == "Language")
		UserLanguage = value;
	else if (section == "User" && name == "Country")
		UserCountry = value;
	else if (section == "Game" && name == "ProductId")
		IniGameProductId = value;
	else if (section == "Override" && name == "ProductName")
		OverrideProductName = value;
	else if (section == "Override" && name == "ProductVersion")
		OverrideProductVersion = value;
	else if (section == "Override" && name == "ProductId")
		OverrideProductId = value;
	else if (section == "Override" && name == "SandboxId")
		OverrideSandboxId = value;
	else if (section == "Override" && name == "ClientId")
		OverrideClientId = value;
	else if (section == "Override" && name == "ClientSecret")
		OverrideClientSecret = value;
	else if (section == "Override" && name == "EncryptionKey")
		OverrideEncryptionKey = value;
	else if (section == "Override" && name == "DeploymentId")
		OverrideDeploymentId = value;
	else if (section == "DLC")
	{
		if (name == "LogQueries")
			DLCLogQueries = StringToBool(value);
		else if (name == "AllOwned")
			DLCAllOwned = StringToBool(value);
		else
			DLCOwned[name] = value;
	}
	else if (section == "ForcedDLC")
	{
		if (name == "UseMalloc")
			ForcedDLCUseMalloc = StringToBool(value);
		else
			DLCForced[name] = value;
	}
	else if (section == "Entitlements")
	{
		Entitlements.push_back(std::make_pair(name, value)); // name = EntitlementId, value = CatalogItemId(DLC ID)
	}
	else
	{
		log(LL::Error, "NEOS_IniHandler: unknown line, section %s, name %s, value %s", section.c_str(), name.c_str(), value);
		return 0;
	}

	return 1;
}

bool IsDllInited = false;

void NEOS_Main()
{
	if (IsDllInited)
		return;

	IsDllInited = true;

	NEOSFolder = GetDllParentDir(NEOSLibrary);

	auto INIPath = NEOSFolder / NEOS_CONFIG_FILE;
	int ret = ini_wparse(INIPath.c_str(), NEOS_IniHandler, 0);

	if (NEOS_RestartAppIfNecessary())
		return;

	log(LL::Info, "nEOS " NEOS_VERSION " by infogram :)");

	if (ret > 0)
		log(LL::Error, "Error reading " NEOS_CONFIG_FILE " - error on line %d", ret);
	else if (ret < 0)
		log(LL::Error, "Error opening " NEOS_CONFIG_FILE " (%d) - using defaults", ret);

	if (FileExists(ProxyFilename.c_str()))
	{
		ProxyLibrary = LoadLibraryA(ProxyFilename.c_str());
		if (ProxyLibrary)
			log(LL::Info, "Proxy mode activated - wrapping DLL %s", ProxyFilename.c_str());
	}
}

BOOL WINAPI DllMain(
	HINSTANCE hinstDLL,  // handle to DLL module
	DWORD fdwReason,     // reason for calling function
	LPVOID lpReserved)  // reserved
{
	// Perform actions based on the reason for calling.
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		// Initialize once for each new process.
		// Return FALSE to fail DLL load.
		NEOSLibrary = hinstDLL;

		// Have to run this in DllMain instead of EOS_Init
		// because game could exit before calling EOS_Init if correct command-line isn't provided
		NEOS_Main();
		break;

	case DLL_THREAD_ATTACH:
		// Do thread-specific initialization.
		//log(LL::Info, "NEOS attached via DLL_THREAD_ATTACH");
		break;

	case DLL_THREAD_DETACH:
		// Do thread-specific cleanup.            
		break;

	case DLL_PROCESS_DETACH:
		// Perform any necessary cleanup.
		break;
	}
	return TRUE;
}
